# -*- coding: utf-8 -*-

# standard python imports
import sys
import pdb
import code
import builtins
import traceback
import threading
import pydoc
import os.path
import logging
try:
    import queue
except ImportError:
    import Queue as queue
from pprint import pformat

# uno python imports
import uno
import unohelper
import pythonscript
import theconsole
import apso_unohelp
from com.sun.star.beans import PropertyValue
from com.sun.star.uno import RuntimeException
from com.sun.star.awt import (XKeyHandler, XWindowListener,  XTextListener, XTopWindowListener,
                              WindowDescriptor, FontDescriptor, Selection, Rectangle)
from com.sun.star.awt.MessageBoxType import (MESSAGEBOX, INFOBOX, ERRORBOX, WARNINGBOX, QUERYBOX)
from com.sun.star.awt.WindowClass import TOP
from com.sun.star.awt.WindowAttribute import MOVEABLE, SIZEABLE, BORDER, CLOSEABLE
from com.sun.star.awt.KeyModifier import SHIFT, MOD1
from com.sun.star.awt.PosSize import SIZE
from com.sun.star.script.provider import ScriptFrameworkErrorException


# ------------LOGGER----------------

LOGLEVEL = {0: logging.WARNING, 1: logging.INFO, 2: logging.DEBUG}
logger = apso_unohelp.logger = logging.getLogger("alternative-python-script-organizer")
formatter = logging.Formatter("%(levelname)s [%(module)s.py][%(funcName)s::%(lineno)d] %(message)s")
if sys.platform != "win32":
    consolehandler = logging.StreamHandler()
    consolehandler.setFormatter(formatter)
    logger.addHandler(consolehandler)
try:
    userpath = uno.getComponentContext().ServiceManager.createInstance(
                    "com.sun.star.util.PathSubstitution").substituteVariables("$(user)", True)
    logfile = os.path.join(uno.fileUrlToSystemPath(userpath), "apso.log")
    filehandler = logging.FileHandler(logfile, mode="w", delay=True)
    filehandler.setFormatter(formatter)
except RuntimeException:
    # At installation time, no context is available -> just ignore it.
    pass

def setlogger(settings):
    loglevel = LOGLEVEL.get(settings["LogLevel"], 0)
    logger.setLevel(loglevel)
    if settings["LogToFile"] == 0:
        logger.removeHandler(filehandler)
    else:
        logger.addHandler(filehandler)


# ------------HELPERS----------------
def createUnoService(service, ctx=None, args=None):
    '''
    Instanciate a Uno service.

    @service: name of the service to be instanciated.
    @ctx: the context if required.
    @args: the arguments when needed.
    '''
    if not ctx:
        ctx = uno.getComponentContext()
    smgr = ctx.getServiceManager()
    if ctx and args:
        return smgr.createInstanceWithArgumentsAndContext(service, args, ctx)
    elif args:
        return smgr.createInstanceWithArguments(service, args)
    elif ctx:
        return smgr.createInstanceWithContext(service, ctx)
    else:
        return smgr.createInstance(service)


def getConfigurationAccess(nodevalue, updatable=False):
    '''
    Access configuration value.

    @nodevalue: the configuration key node as a string.
    @updatable: set True when accessor needs to modify the key value.
    '''
    cp = createUnoService("com.sun.star.configuration.ConfigurationProvider")
    node = PropertyValue("nodepath", 0, nodevalue, 0)
    if updatable:
        return cp.createInstanceWithArguments("com.sun.star.configuration.ConfigurationUpdateAccess", (node,))
    else:
        return cp.createInstanceWithArguments("com.sun.star.configuration.ConfigurationAccess", (node,))


def getProductName():
    '''
    Return the program name.
    '''
    key = "/org.openoffice.Setup/Product"
    reader = getConfigurationAccess(key)
    return reader.ooName


# ------------XRAY---------------

def xray(obj):
    '''
    Uno objects introspection tool.

    Xray is an Uno introspection tool created in basic by Bernard Marcelly.
    This function allows to call it from inside a python script.
    The tool must be installed first. Last version is available at
    http://www.openoffice.org/fr/Documentation/Basic.
    '''
    ctx = uno.getComponentContext()
    url = ("vnd.sun.star.script:XRayTool._Main.Xray?language=Basic&location=application")
    mspf = createUnoService("com.sun.star.script.provider.MasterScriptProviderFactory", ctx)
    try:
        logger.info('Loading Xray...')
        script = mspf.createScriptProvider('').getScript(url)
        script.invoke((obj,), (), ())
    except ScriptFrameworkErrorException:
        logger.debug('Fail loading Xray', exc_info=True)
        msgbox('Xray is not installed.\nPlease visit:\n'
               'http://www.openoffice.org/fr/Documentation/Basic',
               'Error', 'error')


# ------------MRI---------------

def mri(target):
    '''
    Uno objects introspection tool.

    Mri is an Uno introspection tool created in python by Hanya.
    This function allows to call it from inside a python script.
    The tool must be installed first. Last version is available at
    https://github.com/hanya/MRI or at
    https://extensions.libreoffice.org/en/extensions/show/mri-uno-object-inspection-tool.
    '''
    try:
        logger.info('Loading MRIself...')
        mri = createUnoService("mytools.Mri")
        mri.inspect(target)
    except (RuntimeException, AttributeError):
        logger.debug('Fail loading MRI', exc_info=True)
        msgbox('MRI is not installed.\nPlease visit:\n'
               'http://extensions.services.openoffice.org',
               'Error', 'error')


# ------------MSGBOX---------------

def msgbox(message, title="Message", boxtype='message', buttons=1, win=None):
    '''
    Simple message box.

    Like the oobasic build-in function msgbox,
    but simplified as only intended for quick debugging.
    Signature: msgbox(message, title='Message', boxtype='message', buttons=1, win=None).
    '''
    types = {'message': MESSAGEBOX, 'info': INFOBOX, 'error': ERRORBOX,
             'warning': WARNINGBOX, 'query': QUERYBOX}
    tk = createUnoService("com.sun.star.awt.Toolkit")
    if not win:
        desktop = createUnoService("com.sun.star.frame.Desktop")
        frame = desktop.ActiveFrame
        if frame.ActiveFrame:
            # top window is a subdocument
            frame = frame.ActiveFrame
        win = frame.ComponentWindow
    box = tk.createMessageBox(win, types[boxtype], buttons, title, message)
    return box.execute()


# ------------CONSOLE---------------

EOT = b'\x04'


class UnoScriptImporter(object):
    ''' Allow the user to import, from within the console, modules located in AOO/LO containers'''
    def __init__(self, ctx):
        self.ctx = ctx
        self.providers = self._load_providers()
        self.nodes = {}

    def _load_providers(self):
        p = {}
        locations = [u'user', u'user:uno_packages', u'share', u'share:uno_packages']
        for location in locations:
            ext = ""
            if location.endswith('uno_packages'):
                ext = ".oxt"
            p[location] = (pythonscript.PythonScriptProvider(self.ctx, location), ext)
        # FIXME: check if fonctional from base subdocuments
        try:
            doc = XSCRIPTCONTEXT.getDocument()
            if doc.ScriptContainer:
                p['document'] = (pythonscript.PythonScriptProvider(self.ctx, doc), "")
        except (AttributeError, NameError):
            pass
        return p

    def _find_module(self, name, path):
        logger.debug('UnoScriptImporter._find_module')
        logger.debug('name = {}, path = {}'.format(name, path))
        if path:
            node = path[0]
            if node in self.nodes:
                return self._search_node(self.nodes[node], name)
            else:
                return False
        else:
            for prov in self.providers:
                sp, ext = self.providers[prov]
                if self._search_node(sp, name, ext):
                    self.location = prov
                    logger.debug("Found in {}.".format(prov))
                    return True
                logger.debug("Not found in {}.".format(prov))
        return False

    def _search_node(self, node, name, ext=''):
        logger.debug('searching...')
        for child in node.getChildNodes():
            if child.name == uno.systemPathToFileUrl(name+ext):
                self.nodes[self.fullname] = child
                return True
        return False

    def find_module(self, fullname, path=None):
        logger.debug('UnoScriptImporter.find_module')
        logger.debug('fullname: ' + fullname)
        if fullname in ('com',):
            return None
        self.fullname = fullname
        name = fullname.rsplit('.', 1)[-1]
        if self._find_module(name, path):
            return self
        return None

    def _module_from_node(self, node):
        import imp
        mod = imp.new_module(node.name)
        for child in node.getChildNodes():
            try:
                if isinstance(child, pythonscript.FileBrowseNode):
                    setattr(mod, child.name, node.provCtx.getModuleByUrl(child.uri))
                else:
                    setattr(mod, child.name, self._module_from_node(child))
            # import machinery could find a submodule that is
            # not visible from the PythonScriptProvider
            except ImportError:
                logger.exception('Unexpected error while loading module <{}>'.format(child.name))
        return mod

    def load_module(self, fullname):
        logger.debug('UnoScriptImporter.load_module')
        logger.debug('fullname: ' + fullname)
        node = self.nodes[fullname]
        if isinstance(node, pythonscript.DirBrowseNode):
            mod = self._module_from_node(node)
            mod.__file__ = '<{}>'.format(self.location)
            mod.__path__ = [fullname]
            mod.__package__ = fullname
        else:
            mod = node.provCtx.getModuleByUrl(node.uri)
        try:
            parent = fullname.rsplit('.', 1)[-2]
            mod.__package__ = parent
        except IndexError:
            pass
        # name = fullname.rsplit('.', 1)[-1]
        mod.__name__ = fullname
        mod.__loader__ = self
        sys.modules[fullname] = mod
        return mod


class ConsoleWindow(object):
    """Interactive console dialog.

    Instantiate with corresponding keyword parameters to override
    default values: ConsoleWindow(BACKGROUND=0x0, FOREGROUND=0xFFFFFF)>
    """

    FONT = "DejaVu Sans Mono"
    BACKGROUND = 0xFDF6E3
    FOREGROUND = 0x657B83
    MARGIN = 3
    BUTTON_WIDTH = 80
    BUTTON_HEIGHT = 26
    EDIT_HEIGHT = 500
    WIDTH = 800
    PS1 = '>>> '
    PS2 = '... '
    NBTAB = 4
    NPAGING = 25
    HEIGHT = EDIT_HEIGHT + MARGIN * 3

    def __init__(self, prettyprint, **kwargs):
        logger.debug("kwargs = {}".format(kwargs))
        self.banner = None
        for key in kwargs:
            setattr(self, key, kwargs[key])
        if 'ctx' not in kwargs:
            self.ctx = uno.getComponentContext()
        self.smgr = self.ctx.getServiceManager()
        self.setparent = True
        if kwargs.get('parent') is None:
            self.parent = self.getparent()
            self.setparent = False
        self.loc.update(xray=xray, mri=mri, msgbox=msgbox, help=self._help)
        self.title = "APSO console"
        self.edit_name = "console"
        self.product = getProductName()
        self.dialog = None
        self.tk = self.create("com.sun.star.awt.Toolkit")
        self.end = 0
        self.more = 0
        self.history = theconsole.history
        self.historycursor = len(self.history)
        self.npaging = None
        # install display hook for pretty printing in console, if asked for
        self._systemdisplayhook = sys.displayhook
        if prettyprint:
            sys.displayhook = self._customdisplayhook
        else:
            sys.displayhook = self._systemdisplayhook
        # create import hook for macros in AOO/LO containers
        self.importer = UnoScriptImporter(self.ctx)
        logger.debug("UnoScriptImporter added.")
        sys.meta_path.append(self.importer)
        # redirect output to self
        self.stdout = sys.stdout
        self.stderr = sys.stderr
        sys.stdout = self
        sys.stderr = self
        # help() pager
        self.system_getpager = pydoc.getpager
        pydoc.getpager = self._getpager    # make change in pydoc too to apply to builtin help() command
        logger.info("ConsoleWindow initiated.")

    def _help(self, arg=None):
        logger.debug(f"self._help() arg: {repr(arg)[:100]}{len(repr(arg))>100 and '...' or ''}")
        if arg is None:
            help()
        else:
            if not theconsole.inspector:
                theconsole.inspector = self.create("com.sun.star.beans.Introspection")
            if not theconsole.reflector:
                theconsole.reflector = self.create("com.sun.star.reflection.CoreReflection")
            if not theconsole.descriptor:
                theconsole.descriptor = self.ctx.getByName('/singletons/com.sun.star.reflection.theTypeDescriptionManager')
            helptext = apso_unohelp.unohelp(arg)
            if not helptext:
                help(arg)    # default builtin help()
            else:
                try:
                    self._pager(helptext)
                except KeyboardInterrupt:
                    # self.inqueue.put(None)
                    self.inqueue.put(KeyboardInterrupt)

    def _getpager(self):
        # return self._pager    # should be preferred but function name letters are doubled!!
        return pydoc.ttypager

    def _pager(self, text):
        # adapted from pydoc.py
        try:
            lines = text.split('\n')
            pagerprompt = '-- more --'
            if len(lines) >= 30:
                getinput = lambda: sys.stdin.raw_input(pagerprompt)
                r = self.npaging = self.NPAGING - 1
                sys.stdout.write('\n'.join(lines[:self.npaging]) + '\n')
                while lines[r:]:
                    sys.stdout.flush()
                    c = getinput()
                    lastlen = len(pagerprompt) + len(c) + 1

                    if c[:1] in ('q', 'Q'):
                        self._write('\n\n', (self.end - lastlen, self.end))
                        break
                    elif c[:1] in ('b', 'B', '\x1b'):
                        self.npaging = self.NPAGING
                        if r <= self.NPAGING:
                            self._write('\n', (self.end - lastlen, self.end))
                        else:
                            p = r - self.npaging
                            lastpagelen = sum(map(len, lines[p:r])) + self.npaging
                            self._write('\n', (self.end - lastlen - lastpagelen, self.end))
                            r = p
                        continue
                    if c[:1] in ('e', 'E'):
                        self.npaging = len(lines) - r
                    else:
                        try:
                            self.npaging = int(c)
                        except ValueError:
                            pass
                    if self.npaging == 0:
                        # self._write('\n', (self.end - lastlen, self.end))
                        self.npaging = self.NPAGING
                    if self.npaging < 0:
                        p = r + self.npaging
                        if r <= self.NPAGING:
                            self._write('\n', (self.end - lastlen, self.end))
                        else:
                            lastpagelen = sum(map(len, lines[p:r])) - self.npaging
                            self._write('\n', (self.end - lastlen - lastpagelen, self.end))
                            r = p
                    else:
                        self._write('\n' + '\n'.join(lines[r:r+self.npaging]) + '\n',
                            (self.end - lastlen, self.end))
                        r += self.npaging
            else:
                sys.stdout.write(text + "\n")
        except Exception:
            logger.exception('Error in ConsoleWindow._pager')
        finally:
            self.npaging = None

    def _customdisplayhook(self, value):
        if value is None:
            return
        # Set '_' to None to avoid recursion
        builtins._ = None
        self._pager(pformat(value))
        builtins._ = value

    def create(self, name, arguments=None):
        """Create service instance. """
        if arguments:
            return self.smgr.createInstanceWithArgumentsAndContext(name, arguments, self.ctx)
        else:
            return self.smgr.createInstanceWithContext(name, self.ctx)

    def execute(self):
        '''Create dialog and manage context'''
        try:
            self._init()
            self.dialog.setVisible(True)
            proc = threading.Thread(target=_interact,
                                    args=(self.inqueue, self.exitevent, self.PS1, self.PS2, self.product, self.loc))
            proc.start()
            logger.info("Interact threading started.")
            logger.debug('Thread name = {0.name}, thread id = {0.ident}.'.format(proc))
            # self.dialog.execute()
            # self.dialog.dispose()
            logger.info("ConsoleWindow executed.")
        except Exception:
            msgbox(traceback.format_exc())

    def enddialog(self):
        """Terminate dialog"""
        self.dialog.setVisible(False)
        self.dialog.dispose()
        # clean up everything
        theconsole.console = None
        sys.stdout = self.stdout
        sys.stderr = self.stderr
        sys.displayhook = self._systemdisplayhook
        pydoc.getpager = self.system_getpager
        self.inqueue.put(EOT)
        self.tk.removeKeyHandler(self.keyhandler)
        sys.meta_path.remove(self.importer)
        logger.info("ConsoleWindow closed.")

    def create_control(self, win, name, type_, pos, size, prop_names, prop_values):
        """ Create and insert control. """
        model = self.create("com.sun.star.awt.UnoControl{}Model".format(type_))
        if prop_names and prop_values:
            model.setPropertyValues(prop_names, prop_values)
        ctrl = self.create(model.DefaultControl)
        ctrl.setModel(model)
        ctrl.createPeer(win.Toolkit, win)
        ctrl.setPosSize(pos[0], pos[1], size[0], size[1], 15)
        return ctrl

    def create_dialog(self, title, size=None, parent=None):
        """ Create modeless dialog. """
        rect = Rectangle()
        rect.Width, rect.Height = size
        ps = self.parent.getPosSize()
        rect.X, rect.Y = (ps.X + ps.Width - self.WIDTH), ps.Y

        desc = WindowDescriptor()
        desc.Type = TOP
        desc.WindowServiceName = "dialog"
        if self.setparent:
            desc.Parent = self.parent
        desc.ParentIndex = -1
        desc.Bounds = rect
        desc.WindowAttributes = MOVEABLE | SIZEABLE | CLOSEABLE | BORDER

        dialog = self.tk.createWindow(desc)
        dialog.setTitle(title)

        self.dialog = dialog

    def create_edit(self, name, pos, size, prop_names=None, prop_values=None):
        """ Create and add new edit control. """
        self.edit = self.create_control(self.dialog, name, "Edit", pos, size, prop_names, prop_values)

    def getparent(self):
        '''Returns parent frame'''
        desktop = self.create("com.sun.star.frame.Desktop")
        return desktop.CurrentFrame.ContainerWindow

    class ListenerBase(unohelper.Base):
        def __init__(self, act):
            self.act = act

        def disposing(self, source):
            self.act = None

    class WindowListener(ListenerBase, XWindowListener):
        def windowResized(self, e):
            size = e.Source.Size
            margin = self.act.MARGIN
            self.act.edit.setPosSize(0, 0, size.Width - margin*2, size.Height - margin*3, SIZE)

        # unused events
        def windowMoved(self, e):
            pass

        def windowShown(self, e):
            pass

        def windowHidden(self, e):
            pass

    class TopWindowListener(ListenerBase, XTopWindowListener):
        def windowClosing(self, e):
            self.act.enddialog()

        def windowDeactivated(self, e):
            self.act.tk.removeKeyHandler(self.act.keyhandler)

        def windowActivated(self, e):
            self.act.tk.addKeyHandler(self.act.keyhandler)

        # unused events
        def windowOpened(self, e):
            pass

        def windowClosed(self, e):
            pass

        def windowMinimized(self, e):
            pass

        def windowNormalized(self, e):
            pass

    class TextListener(ListenerBase, XTextListener):
        def textChanged(self, ev):
            self.act.end = len(ev.Source.Text)

    class KeyHandler(ListenerBase, XKeyHandler):
        def keyPressed(self, ev):
            try:
                ret = getattr(self.act, "onkey_" + str(ev.KeyCode))(ev.Modifiers)
                if ret == 1:
                    return 1
            except AttributeError:
                # logger.debug("No special behaviour associated.", exc_info=True)
                pass
            line = self.act._readline()
            if self.act.edit.Selection.Min >= (self.act.end-len(line)):
                return 0
            return 1

        def keyReleased(self, ev):
            return 0

    def onkey_514(self, modifiers):
        '''Catch ctrl+C keyboard entry'''
        if (modifiers & MOD1):
            sel = self.edit.Selection
            if sel.Min == sel.Max:
                self._keyboardinterrupt()
            return 1
        return 0

    def onkey_515(self, modifiers):
        '''Catch ctrl+D keyboard entry'''
        if (modifiers & MOD1):
            try:
                self.enddialog()
                return 1
            except Exception:
                traceback.print_exc()
                self._write(self.PS1)
                return 1
        return 0

    def onkey_537(self, modifiers):
        '''Catch ctrl+Z keyboard entry'''
        if (modifiers & MOD1):
            try:
                self.enddialog()
                return 1
            except Exception:
                traceback.print_exc()
                self._write(self.PS1)
                return 1
        return 0

    def onkey_1024(self, modifiers):
        '''Catch DOWN keyboard entry'''
        if  self.npaging is not None:
            self.npaging = abs(self.npaging)
            self.onkey_1280(modifiers)
            return 1
        try:
            line = self._readline()
            if self.historycursor < len(self.history)-1:
                self.historycursor += 1
                self._write(self.history[self.historycursor], (self.end-len(line), self.end))
            else:
                self.historycursor = len(self.history)
                self._write("", (self.end-len(line), self.end))
            self.gotoendofinput()
            return 1
        except Exception:
            self.inqueue.put(traceback.format_exc())

    def onkey_1025(self, modifiers):
        '''Catch UP keyboard entry'''
        if  self.npaging is not None:
            self.npaging = -abs(self.npaging)
            self.onkey_1280(modifiers)
            return 1
        try:
            line = self._readline()
            if self.historycursor > 0:
                self.historycursor -= 1
                self._write(self.history[self.historycursor], (self.end-len(line), self.end))
            self.gotoendofinput()
            return 1
        except Exception:
            self.inqueue.put(traceback.format_exc())

    def onkey_1026(self, modifiers):
        '''Catch LEFT keyboard entry'''
        line = self._readline()
        if self.edit.Selection.Min <= (self.end-len(line)):
            self.gotostartofinput()
            return 1
        return 0

    def onkey_1027(self, modifiers):
        '''Catch RIGHT keyboard entry'''
        line = self._readline()
        if self.edit.Selection.Min < (self.end-len(line)):
            self.gotoendofinput()
            return 1
        return 0

    def onkey_1028(self, modifiers):
        '''Catch HOME keyboard entry'''
        if not (modifiers & SHIFT):
            self.gotostartofinput()
            return 1
        return 0

    def onkey_1029(self, modifiers):
        '''Catch END keyboard entry'''
        if not (modifiers & SHIFT):
            self.gotoendofinput()
            return 1
        return 0

    def onkey_1280(self, modifiers):
        '''Catch RETURN keyboard entry'''
        try:
            line = self._readline()
            if line and self.npaging is None:
                if not self.history or line != self.history[-1]:
                    self.history.append(line)
                self.historycursor = len(self.history)
            if self.edit.Selection.Max > (self.end-len(self.prompt+line)):
                if line in ("clear", "clear()"):
                    self.clear()
                else:
                    if self.npaging is None:
                        self._write("\n")
                    self.prompt = ""
                    self.inqueue.put(line)
            self.gotoendofinput()
            return 1
        except Exception:
            self.inqueue.put(traceback.format_exc())

    def onkey_1281(self, modifiers):  # ESCAPE
        '''Catch ESCAPE keyboard entry'''
        return 1

    def onkey_1282(self, modifiers):  # TAB
        '''Catch TAB keyboard entry'''
        if not modifiers:
            self._write(' ' * self.NBTAB)
            return 1
        return 0

    def onkey_1283(self, modifiers):
        '''Catch BACKSPACE keyboard entry'''
        line = self._readline()
        if self.edit.Selection.Min <= (self.end-len(line)):
            return 1
        return 0

    def _keyboardinterrupt(self):
        '''Send KeyboardInterror exception.
        This exception will not allways work as expected'''
        self.inqueue.put(KeyboardInterrupt)

    def gotoendofinput(self):
        '''Send visible cursor to end of input line'''
        self.edit.setSelection(Selection(self.end, self.end))

    def gotostartofinput(self):
        '''Send visible cursor to start of input line'''
        line = self._readline()
        pos = self.end - len(line)
        self.edit.setSelection(Selection(pos, pos))

    def clear(self):
        '''Clear edit area'''
        self.edit.Text = self.prompt

    def _init(self):
        margin = self.MARGIN
        font = FontDescriptor()
        font.Name = self.FONT
        self.create_dialog(self.title, size=(self.WIDTH, self.HEIGHT))
        self.dialog.addWindowListener(self.WindowListener(self))
        self.dialog.addTopWindowListener(self.TopWindowListener(self))
        self.create_edit(self.edit_name,
                         pos=(margin, margin * 2),
                         size=(self.WIDTH - margin * 2, self.EDIT_HEIGHT),
                         prop_names=("AutoVScroll", "BackgroundColor", "FontDescriptor", "HideInactiveSelection",
                                     "MultiLine", "TextColor"),
                         prop_values=(True, self.BACKGROUND, font, True, True, self.FOREGROUND))

        self.keyhandler = self.KeyHandler(self)
        self.edit.addTextListener(self.TextListener(self))
        self.edit.setFocus()
        logger.info("ConsoleWindow gui created.")

    def flush(self):
        '''Does it need to be implemented?'''
        pass

    def write(self, data):
        '''Implements sys.stdout/stderr write method'''
        try:
            if self.exitevent.is_set():
                self.enddialog()
                return
            self.prompt = data.split("\n")[-1]
            self._write(data)
        except Exception:
            logger.debug("Error on ConsoleWindow.write", exc_info=True)
            msgbox("Error on ConsoleWindow.write:\n" + traceback.format_exc())

    def _write(self, data, sel=None):
        '''Append data to edit control text'''
        if not sel:
            sel = (self.end, self.end)
        self.edit.insertText(Selection(*sel), data)

    def _readline(self):
        '''Returns input text'''
        lines = self.edit.Text.split("\n")
        lastinput = []
        for line in lines[::-1]:
            lastinput.append(line)
            if line.startswith(self.prompt):
                break
        # xray(lastinput)
        line = lines[-1]
        return line[len(self.prompt):]


class Interact(code.InteractiveConsole):
    def __init__(self, inqueue, exitevent, ps1, ps2, product, loc=None):
        code.InteractiveConsole.__init__(self, loc)
        self.inqueue = inqueue
        self.exitevent = exitevent
        self.ps1, self.ps2 = ps1, ps2
        self.product = product
        self.keep_prompting = True
        self.stdin = sys.stdin
        # sys.stdin = self
        logger.info("Console Interact initiated.")

    def readline(self):
        '''Implements sys.stdin readline method'''
        rl = self.inqueue.get()
        logger.debug("inqueue get '{}'".format(rl))
        if rl is KeyboardInterrupt:
            raise KeyboardInterrupt()
        if rl == EOT:
            self.keep_prompting = False
            return ''
        return rl + '\n'

    def interact(self, banner=None):
        '''Overwrite default interact method'''
        cprt = 'Type "help", "copyright", "credits" or "license" for more information.'
        if banner is None:
            self.write("APSO python console [{}]\n{}\n{}\n".format(self.product, sys.version, cprt))
        elif banner:
            self.write("%s\n" % str(banner))
        more = 0
        try:
            while self.keep_prompting:
                # sys.stdin is sometime lost in OpenOffice and must be repeated
                sys.stdin = self
                try:
                    if more:
                        prompt = self.ps2
                    else:
                        prompt = self.ps1
                    line = self.raw_input(prompt)
                    more = self.push(str(line))
                except KeyboardInterrupt:
                    logger.debug('Keyboard interruption (ctrl+C).', exc_info=True)
                    self.write("\nKeyboardInterrupt\n")
                    self.resetbuffer()
                    more = 0
                except SystemExit:
                    logger.debug('Exit event raised.', exc_info=True)
                    self.exitevent.set()
                except EOFError as e:
                    logger.info('{} (console closing)'.format(e))
                    self.resetbuffer()
                    more = 0
                except Exception:
                    logger.exception('Unknown interact error.')
                    self.resetbuffer()
                    more = 0
        finally:
            sys.stdin = self.stdin


def _interact(*args):
    iconsole = Interact(*args)
    iconsole.interact()
    # msgbox('stop')


def console(prettyprint, **kwargs):
    '''
    Launch the "python interpreter" gui.

    Keyword arguments are:
    - 'loc': for passing caller's locales and/or globals to the console context
    - any constructor constant (BACKGROUND, FOREGROUND...) to tweak the console aspect.

    Examples:
    - console()  # defaut constructor)
    - console(loc=locals())
    - console(BACKGROUND=0x0, FOREGROUND=0xFFFFFF)

    More infos: https://extensions.libreoffice.org/en/extensions/show/apso-alternative-script-organizer-for-python.
    '''

    try:
        if theconsole.console:
            theconsole.tofront()
            # hack a little bug within OpenOffice
            sys.stdout = sys.stderr = theconsole.console
            return
        loc = kwargs.setdefault('loc', {})
        loc.update(pdb=pdb)
        kwargs.update(inqueue=queue.Queue(), exitevent=threading.Event())
        console_ = ConsoleWindow(prettyprint, **kwargs)
        theconsole.console = console_
        console_.execute()
        return console_
    except Exception:
        msgbox(traceback.format_exc())
        return None
