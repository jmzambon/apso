# -*- coding: utf-8 -*-

console = None
history = []
inspector = None
reflector = None
descriptor = None


def tofront():
    console.dialog.setVisible(True)
    console.dialog.toFront()


def close():
    console.enddialog()
