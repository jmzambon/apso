# -*- coding: utf-8 -*-

import inspect
import theconsole
from com.sun.star.beans.MethodConcept import ALL as METHOD_ALL
from com.sun.star.beans.PropertyConcept import PROPERTYSET, ATTRIBUTES, METHODS
from com.sun.star.reflection.MethodMode import ONEWAY
# from com.sun.star.lang import NoSuchMethodException
from com.sun.star.uno.TypeClass import INTERFACE, STRUCT, ENUM


PROPATTRIBUTES = {
    0x1: "can be void",
    0x2: "bound",
    0x4: "constraint",
    0x8: "not persistent",
    0x10: "read-only",
    0x20: "can be ambiguous",
    0x40: "can be set to default",
    0x80: "can be removed",
    0x100: "optional"}


logger = None


def unohelp(unoobject, inseq=False):

    def unoenumhelp():
        enumname = refl_obj.Name
        enum = descriptor.getByHierarchicalName(enumname)
        members = []
        for name, value in zip(enum.EnumNames, enum.EnumValues):
            members.append(f"{name} \n│\t\tmember value: {value} ({hex(value)})")

        ret = ("UNO enum < {} >\n\n│\n├─ {}\n".format(
            enumname,
            "\n│\n│\n├─ ".join(members)))
        return ret

    def unostructhelp():
        members = []
        for field in refl_obj.Fields:
            members.append(f"{field.Name}\n│\t\tmember type: {field.Type.Name}")
            # members.append(f"{field.Name} [<- {field.DeclaringClass.Name}]\n│\t\tmember type: {field.Type.Name}")
        ret = ("UNO {}struct < {} >\n\n│\n├─ {}\n".format(
            inseq and "[]" or "",
            refl_obj.Name,
            "\n│\n│\n├─ ".join(members)))
        return ret

    def unointerfacehelp():
        insp_obj = inspector.inspect(unoobject)
        # xray(insp_obj); return
        objectproperties = insp_obj.getProperties(insp_obj.SuppliedPropertyConcepts)
        objectproperties = sorted(objectproperties, key=lambda x: x.Name)
        properties = []
        attributes = []
        pseudoproperties = []
        for member in objectproperties:
            if insp_obj.hasProperty(member.Name, PROPERTYSET):
                helptext = getpropdetails(member, PROPERTYSET)
                properties.append(helptext)
            elif insp_obj.hasProperty(member.Name, METHODS):
                helptext = getpropdetails(member, METHODS, insp_obj)
                pseudoproperties.append(helptext)
            elif insp_obj.hasProperty(member.Name, ATTRIBUTES):
                helptext = getpropdetails(member, ATTRIBUTES)
                attributes.append(helptext)
        objectmethods = insp_obj.getMethods(insp_obj.SuppliedMethodConcepts)
        objectmethods = sorted(objectmethods, key=lambda x: x.Name)
        methods = []
        for member in objectmethods:
            helptext = getmethoddetails(member)
            methods.append(helptext)

        ret = ("UNO {}interface <{}>\n\n│\n├─ "
               "ATTRIBUTES\n│\n│\t{}\n│\n├─ "
               "PROPERTIES\n│\n│\t{}\n│\n├─ "
               "PSEUDOPROPERTIES (get/set)\n│\n│\t{}\n│\n├─ "
               "METHODS\n│\n│\t{}\n".format(
                    inseq and "[]" or "",
                    reflector.getType(unoobject).Name,
                    '\n│\t'.join(attributes) or "(none)\n│",
                    '\n│\t'.join(properties) or "(none)\n│",
                    '\n│\t'.join(pseudoproperties) or "(none)\n│",
                    '\n│\n│\t'.join(methods) or "(none)"))
        return ret

    try:
        logger.debug("Testing logger from apso_unohelp.py.")

        inspector = theconsole.inspector
        reflector = theconsole.reflector
        descriptor = theconsole.descriptor

        if isinstance(unoobject, tuple):
            if unoobject == ():
                return None
            else:
                return unohelp(unoobject[0], True)

        # UNO METHOD
        elif type(unoobject).__name__ == "PyUNO_callable":
            callertext = theconsole.history[-1]
            callerlocals = inspect.currentframe().f_back.f_back.f_locals
            objects = callertext.strip(')\n ').split('(')[-1]
            objects = objects.strip().split('.')
            baseobject = callerlocals[objects.pop(0)]
            if not objects:
                return "\n".join(dir(baseobject))
            else:
                method = objects.pop(-1)
            for obj in objects:
                baseobject = getattr(baseobject, obj)

            insp_obj = inspector.inspect(baseobject)
            idlmethod = insp_obj.getMethod(method, METHOD_ALL)
            helptext = getmethoddetails(idlmethod)
            ret = "UNO interface <{}>\n\n│\n├─ \tmethod {}".format(
                    reflector.getType(baseobject).Name,
                    helptext)
            return ret

        else:
            ret = ""
            try:
                refl_obj = reflector.getType(unoobject)
            except Exception:
                logger.exception("Exception on reflector.getType -> return None")
                return ret
            typeclass = refl_obj.TypeClass
            # UNO interface
            if typeclass == INTERFACE:
                logger.debug("Help requested on uno interface.")
                ret = unointerfacehelp()
            # UNO struct
            elif typeclass == STRUCT:
                logger.debug("Help requested on uno struct.")
                ret = unostructhelp()
            # UNO enum
            elif typeclass == ENUM:
                logger.debug("Help requested on uno enum.")
                ret = unoenumhelp()
            return ret

    except Exception:
        logger.exception('')
        return None


def getpropdetails(prop, concept, insp_obj=None):
    propdetails = prop.Name
    attributes = []
    for attribute, value in PROPATTRIBUTES.items():
        if prop.Attributes & attribute:
            attributes.append(value)
    propdetails += f"\n│\t\t\ttype: {prop.Type.typeName}\n│"
    if attributes:
        propdetails += f"\t\t\tattributes: {','.join(attributes)}\n│"
    if concept == METHODS:
        methods = []
        if insp_obj.hasMethod('get'+prop.Name, METHOD_ALL):
            methods.append(f'get{prop.Name}()')
        if insp_obj.hasMethod('set'+prop.Name, METHOD_ALL):
            methods.append(f'set{prop.Name}()')
        propdetails += f"\t\t\tidl methods: {', '.join(methods)}\n│"
    return propdetails


def getmethoddetails(idlmethod):
    definition = {}
    definition['mode'] = idlmethod.Mode == ONEWAY and 'one way' or 'two way'
    definition['name'] = idlmethod.Name

    returntype = idlmethod.ReturnType
    definition['returntypename'] = returntype.Name
    # ~ definition['returntypeclass'] = f"{returntype.TypeClass.typeName}.{returntype.TypeClass.value}"
    definition['declaringclass'] = idlmethod.DeclaringClass.Name

    parameters = []
    paraminfos = idlmethod.ParameterInfos
    paramtypes = idlmethod.ParameterTypes
    for paraminfo, paramtype in zip(paraminfos, paramtypes):
        param = {}
        param['name'] = paraminfo.aName
        param['mode'] = paraminfo.aMode.value.lower()
        param['type'] = paramtype.Name
        parameters.append(param)
    definition['parameters'] = parameters

    exceptiontypes = []
    for exception in idlmethod.ExceptionTypes:
        exceptiontypes.append('\n│\t\t\t\t' + exception.Name)

    paramstrings = []
    none = "\n│\t\t\t\t(none)"
    for param in parameters:
        paramstrings.append(f"\n│\t\t\t\t[{param['mode']}] {param['name']} ({param['type']})")

    methoddetails = (f"{definition['name']}() [<- {definition['declaringclass']}]"
                     f"\n│\t\t\tParameters: {''.join(paramstrings) or none}"
                     "\n│\t\t\tReturns:"
                     f"\n│\t\t\t\t{definition['returntypename'] or 'void'}"
                     f"\n│\t\t\tRaises: {''.join(exceptiontypes) or none}")
    return methoddetails
