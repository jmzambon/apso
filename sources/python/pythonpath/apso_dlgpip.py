# coding: utf-8

import os
import sys
import platform
import tempfile
import re
import subprocess
import unohelper
from threading import Thread
from importlib.metadata import distribution
# from apso_utils import msgbox
from com.sun.star.awt import XActionListener, XTextListener, XWindowListener
from com.sun.star.awt.PushButtonType import OK as PBT_OK
from com.sun.star.util.MeasureUnit import APPFONT
from com.sun.star.style.VerticalAlignment import MIDDLE as VA_MIDDLE


class dlgPip_UI(unohelper.Base, XActionListener, XTextListener, XWindowListener):
    """
    Class documentation...
    """

    MARGIN = 5
    BASE_WIDTH = 60
    BASE_HEIGHT = 14
    CONS_HEIGHT = BASE_HEIGHT * 6 + MARGIN * 6
    CONS_WIDTH = 0    # if 0 --> to be calculated later
    DLG_WIDTH = 0
    DLG_HEIGHT = 0
    TXTPKG_WIDTH = 0
    INSTPKG_X = 0
    RCTRL_X = 0
    CMDCLEAR_Y = 0
    CMDOK_Y = 0

    def __init__(self, ctx, dlgPip_str):

        self.DicoStr = {name: (label, helptext) for name, label, helptext in dlgPip_str}
        # self.longest_string = max(self.DicoStr['cmdUninstPkg'], self.DicoStr['cmdListPkg'], self.DicoStr['cmdInstPip'], self.DicoStr['cmdClear'], self.DicoStr['cmdOK'], key=len)
        # print(self.longest_string)
        self.LocalContext = ctx
        self.ServiceManager = self.LocalContext.ServiceManager
        desktop = self.ServiceManager.createInstance("com.sun.star.frame.Desktop")
        self.parentwin = desktop.ActiveFrame.ContainerWindow
        self._build_UI()

    def _build_UI(self):

        # -----------------------------------------------------------
        #               Create dialog and insert controls
        # -----------------------------------------------------------

        def createcontrol(model, name, X, Y, width=None, height=None, command=None, **kwargs):
            ctrlmodel = self.DialogModel.createInstance(f"com.sun.star.awt.UnoControl{model}Model")
            ctrlmodel.Name = name
            ctrlmodel.PositionX = X
            ctrlmodel.PositionY = Y
            for prop, value in kwargs.items():
                ctrlmodel.setPropertyValue(prop, value)
            if model in ('FixedText', 'Button'):
                ctrlmodel.Label = self.DicoStr[name][0]
            try:
                ctrlmodel.HelpText = self.DicoStr[name][1]
            except KeyError:
                pass
            self.DialogModel.insertByName(name, ctrlmodel)
            control = self.DialogContainer.getControl(name)
            minsize = convertsizetologic(control.MinimumSize, APPFONT)
            ctrlmodel.Width = width or minsize.Width
            ctrlmodel.Height = height or minsize.Height
            if command:
                control.addActionListener(self)
                control.setActionCommand(command)
            return ctrlmodel

        # --------------create dialog container and set model and properties
        self.DialogContainer = self.ServiceManager.createInstance("com.sun.star.awt.UnoControlDialog")
        self.DialogModel = self.ServiceManager.createInstance("com.sun.star.awt.UnoControlDialogModel")
        self.DialogContainer.setModel(self.DialogModel)
        self.DialogModel.Name = "dlgPip"
        # # self.DialogModel.PositionX = 81
        # self.DialogModel.PositionY = 63
        self.DialogModel.Width = self.DLG_WIDTH
        self.DialogModel.Height = self.DLG_HEIGHT
        self.DialogModel.HelpText = self.DicoStr['dlgPip'][1]
        self.DialogModel.Closeable = True
        self.DialogModel.Moveable = True
        self.DialogModel.Sizeable = True
        convertsizetologic = self.parentwin.convertSizeToLogic
        # self.DialogContainer.Title = self.DicoStr['dlgPip'][0]

        # add a window listener for resize events
        self.DialogContainer.addWindowListener(self)

        # --------- create an instance of FixedText control, set properties ---
        lblPkg = createcontrol("FixedText", "lblPkg", self.MARGIN, self.MARGIN,
                               height=self.BASE_HEIGHT, Align=2, VerticalAlign=VA_MIDDLE)

        # --------- create an instance of Edit control, set properties ---
        posX = lblPkg.Width + 2 * self.MARGIN
        self.TXTPKG_WIDTH = self.BASE_WIDTH * 2
        self.txtPkg = createcontrol("Edit", "txtPkg", posX, self.MARGIN, width=self.TXTPKG_WIDTH,
                                    height=self.BASE_HEIGHT, TabIndex=1, BackgroundColor=0x00ffdbb6)

        # # add the text listener
        # if isPipInstalled():
        #     self.DialogContainer.getControl('txtPkg').addTextListener(self)

        # --------- create an instance of Button control, set properties ---
        self.INSTPKG_X = self.txtPkg.PositionX + self.txtPkg.Width + self.MARGIN
        self.cmdInstPkg = createcontrol("Button", "cmdInstPkg", self.INSTPKG_X, self.MARGIN,
                                        height=self.BASE_HEIGHT, command='cmdInstPkg_OnClick', TabIndex=2)

        # --------- create an instance of Button control, set properties ---
        self.RCTRL_X = self.INSTPKG_X + self.cmdInstPkg.Width + self.MARGIN
        self.cmdUninstPkg = createcontrol("Button", "cmdUninstPkg", self.RCTRL_X, self.MARGIN,
                                          height=self.BASE_HEIGHT, command='cmdUninstPkg_OnClick', TabIndex=3)

        # --------- create an instance of Button control, set properties ---
        posY = self.BASE_HEIGHT + self.MARGIN * 3
        self.cmdListPkg = createcontrol("Button", "cmdListPkg", self.RCTRL_X, posY,
                                        height=self.BASE_HEIGHT, command='cmdListPkg_OnClick', TabIndex=4)

        # --------- create an instance of Button control, set properties ---
        posY = self.BASE_HEIGHT * 2 + self.MARGIN * 4
        self.cmdInstPip = createcontrol("Button", "cmdInstPip", self.RCTRL_X, posY,
                                        height=self.BASE_HEIGHT, command='cmdInstPip_OnClick', TabIndex=5, FocusOnClick=False)

        # --------- create an instance of Edit control, set properties ---
        self.CONS_WIDTH = self.RCTRL_X - 2 * self.MARGIN
        self.txtConsole = createcontrol("Edit", "txtConsole", self.MARGIN, self.cmdListPkg.PositionY,
                                        width=self.CONS_WIDTH, height=self.CONS_HEIGHT, TabIndex=6,
                                        BackgroundColor=0x00fff5ce, MultiLine=True, HScroll=True, AutoVScroll=True)
        self.DialogContainer.getControl("txtConsole").setEditable(False)

        # --------- create an instance of Button control, set properties ---
        self.CMDOK_Y = self.txtConsole.PositionY + self.txtConsole.Height - self.BASE_HEIGHT
        self.cmdOK = createcontrol("Button", "cmdOK", self.RCTRL_X, self.CMDOK_Y,
                                   height=self.BASE_HEIGHT, command='cmdOK_OnClick', TabIndex=8)
        self.cmdOK.PushButtonType = PBT_OK    # problem setting that property with setPropertyValue

        # --------- create an instance of Button control, set properties ---
        self.CMDCLEAR_Y = self.CMDOK_Y - self.BASE_HEIGHT - self.MARGIN
        self.cmdClear = createcontrol("Button", "cmdClear", self.RCTRL_X, self.CMDCLEAR_Y,
                                      height=self.BASE_HEIGHT, command='cmdClear_OnClick', TabIndex=7)

        # --------- adlusting various sizes ---
        # adjusting right controls width
        rightcontrols = (self.cmdUninstPkg, self.cmdListPkg, self.cmdInstPip, self.cmdClear, self.cmdOK)
        maxrightwidth = max(ctrl.Width for ctrl in rightcontrols)
        for control in rightcontrols:
            control.Width = maxrightwidth

        # adjusting dialog possize
        self.DLG_WIDTH = sum((lblPkg.Width, self.txtPkg.Width, self.cmdInstPkg.Width, maxrightwidth, 5 * self.MARGIN))
        self.DLG_HEIGHT = sum((self.BASE_HEIGHT, self.txtConsole.Height, 4 * self.MARGIN))
        self.DialogModel.Width = self.DLG_WIDTH
        self.DialogModel.Height = self.DLG_HEIGHT

        ps = self.parentwin.getPosSize()
        dlg_size = self.DialogContainer.Size
        self.DialogContainer.setPosSize((ps.Width - dlg_size.Width)/2, (ps.Height - dlg_size.Height)/4, 0, 0, 3)

    # -----------------------------------------------------------
    #               All listener
    # -----------------------------------------------------------

    def disposing(self, source):
        pass

    # -----------------------------------------------------------
    #               Window listener
    # -----------------------------------------------------------

    def windowResized(self, event):
        dlg_width = event.Source.Model.Width
        dlg_height = event.Source.Model.Height
        diff_width = dlg_width - self.DLG_WIDTH
        diff_height = dlg_height - self.DLG_HEIGHT

        self.txtConsole.Width = self.CONS_WIDTH + diff_width
        self.txtConsole.Height = self.CONS_HEIGHT + diff_height
        self.txtPkg.Width = self.TXTPKG_WIDTH + diff_width
        self.cmdInstPkg.PositionX = self.INSTPKG_X + diff_width
        self.cmdUninstPkg.PositionX = self.RCTRL_X + diff_width
        self.cmdListPkg.PositionX = self.RCTRL_X + diff_width
        self.cmdInstPip.PositionX = self.RCTRL_X + diff_width
        self.cmdClear.PositionX = self.RCTRL_X + diff_width
        self.cmdClear.PositionY = self.CMDCLEAR_Y + diff_height
        self.cmdOK.PositionX = self.RCTRL_X + diff_width
        self.cmdOK.PositionY = self.CMDOK_Y + diff_height

    def windowMoved(self, event):
        pass

    def windowShown(self, event):
        pass

    def windowHidden(self, event):
        pass

    # -----------------------------------------------------------
    #               Text listener
    # -----------------------------------------------------------

    def textChanged(self, textevent):
        # intended for future development
        pass

    # -----------------------------------------------------------
    #               Action events
    # -----------------------------------------------------------

    def actionPerformed(self, oActionEvent):

        if oActionEvent.ActionCommand == 'cmdOK_OnClick':
            self.cmdOK_OnClick()

        if oActionEvent.ActionCommand == 'cmdClear_OnClick':
            self.cmdClear_OnClick()

        if oActionEvent.ActionCommand == 'cmdInstPip_OnClick':
            self.cmdInstPip_OnClick()

        if oActionEvent.ActionCommand == 'cmdListPkg_OnClick':
            self.cmdListPkg_OnClick()

        if oActionEvent.ActionCommand == 'cmdInstPkg_OnClick':
            self.cmdInstPkg_OnClick()

        if oActionEvent.ActionCommand == 'cmdUninstPkg_OnClick':
            self.cmdUninstPkg_OnClick()


class dlgPip(dlgPip_UI):
    '''
    Class documentation...
    '''
    def __init__(self, ctx, dlgPip_str, logger):
        if platform.system() == "Windows":
            # self.pythonexe = os.path.join( os.path.dirname(sys.executable),'python.exe')
            # self.pythonexe = os.path.join(Path(os.__file__).parents[2].as_posix(),'python.exe')
            self.pythonexe = find_python()
            self.shell = True
        else:
            # self.pythonexe = os.path.join(Path(os.__file__).parents[2].as_posix(),'python')
            # self.pythonexe = os.path.join( os.path.dirname(sys.executable),'python3')
            self.pythonexe = find_python()
            self.shell = False
        self.ctx = ctx
        self.logger = logger
        dlgPip_UI.__init__(self, ctx, dlgPip_str)

        # for key, value in kwargs.items():
        #   if key == 'document':
        #   self.document = value

        # --------- my code ---------------------

        self.DialogModel.Title = "dlgPip"
        # mri(self.ctx, self.DialogContainer)

    def myFunction(self):
        # TODO: not implemented
        pass

    # -----------------------------------------------------------
    #               Execute dialog
    # -----------------------------------------------------------

    def showDialog(self):
        self.DialogContainer.setVisible(True)
        self.DialogContainer.createPeer(self.parentwin.Toolkit, None)
        self.DialogContainer.Title = self.DicoStr['dlgPip'][0]
        self.enableButtons()
        self.DialogContainer.execute()

    def disableDialog(self):
        self.DialogModel.Enabled = False

    def enableButtons(self):
        self.DialogModel.Enabled = True
        if isPipInstalled():
            self.cmdInstPip.Enabled = False
            self.cmdListPkg.Enabled = True
            self.cmdInstPkg.Enabled = True
            self.cmdUninstPkg.Enabled = True
            self.txtPkg.Enabled = True
            self.DialogContainer.getControl("txtPkg").setFocus()
        else:
            self.cmdInstPip.Enabled = True
            self.cmdListPkg.Enabled = False
            self.cmdInstPkg.Enabled = False
            self.cmdUninstPkg.Enabled = False
            self.txtPkg.Enabled = False

    # -----------------------------------------------------------
    #               Action events
    # -----------------------------------------------------------

    def cmdOK_OnClick(self):
        pass
        # self.DialogModel.Title = "It's Alive! - cmdOK"
        # msgbox("It's Alive! - cmdOK", "Event: OnClick", "info")
        # TODO: not implemented

    def cmdClear_OnClick(self):
        # self.DialogModel.Title = "It's Alive! - CmdClear"
        # msgbox("It's Alive! - CmdClear", "Event: OnClick", "info")
        self.txtConsole.Text = ''
        self.txtConsole.TextColor = -1
        # TODO: not implemented
        self.txtPkg.Text = ''
        self.DialogContainer.getControl("txtPkg").setFocus()

    def cmdInstPip_OnClick(self):
        self.t = Thread(target=self.workerCmdInstPip)  # , args = (doc,))
        self.t.start()

    def workerCmdInstPip(self):
        self.disableDialog()
        try:
            # self.DialogModel.Title = "It's Alive! - cmdInstPip"
            # msgbox("It's Alive! - cmdInstPip", "Event: OnClick", "info")
            getpipPython = "https://bootstrap.pypa.io//pip/2.7/get-pip.py"
            getpipPython3 = "https://bootstrap.pypa.io/get-pip.py"
            getpipFile = os.path.join(tempfile.gettempdir(), 'get-pip.py')
            self.logger.debug(f'getpipFile = {getpipFile}')
            cons = self.txtConsole
            cons.TextColor = -1
            cons.Text = ""
            import ssl
            ssl._create_default_https_context = ssl._create_unverified_context
            try:
                from urllib import urlretrieve  # Python 2.X
            except ImportError:
                from urllib.request import urlretrieve   # Python 3+
            try:
                cons.Text += "Downloading get-pip.py file for python " + str(sys.version_info) + '\n'
                if sys.version_info < (3, 7):
                    urlretrieve(getpipPython, getpipFile)
                else:
                    urlretrieve(getpipPython3, getpipFile)
            except Exception as e:
                cons.Text += "error downloading get-pip script : " + str(e) + '\n'
            try:
                cons.Text += "Executing get-py.py script. Please wait...\n"
                output = subprocess.check_output(
                    [self.pythonexe, getpipFile, "--user", "--no-warn-script-location"],
                    stderr=subprocess.STDOUT, shell=self.shell)
                cons.Text = output
            except Exception as e:
                cons.Text = e.output
        finally:
            self.enableButtons()
        # TODO: not implemented

    def cmdListPkg_OnClick(self):
        self.t = Thread(target=self.workerCmdListPkg)  # , args = (doc,))
        self.t.start()

    def workerCmdListPkg(self):
        self.disableDialog()
        # self.DialogModel.Title = "It's Alive! - CmdLister"
        # msgbox("It's Alive! - CmdLister", "Event: OnClick", "info")
        cons = self.txtConsole
        cons.TextColor = -1
        cons.Text = "List Packages in progress. Please wait ...\n"
        try:
            self.logger.debug(f'subprocessing <pip list --user> (pythonexe: {self.pythonexe}, shell: {self.shell})')
            output = subprocess.check_output(
                [self.pythonexe, "-m", "pip", "list", "--user"], stderr=subprocess.STDOUT, shell=self.shell)
            cons.Text = output
        except subprocess.CalledProcessError as e:
            cons.Text = e.output
        finally:
            self.enableButtons()
        # TODO: not implemented

    def cmdInstPkg_OnClick(self):
        self.t = Thread(target=self.workerCmdInstPkg)  # , args = (doc,))
        self.t.start()

    def workerCmdInstPkg(self):
        # self.DialogModel.Title = "It's Alive! - CmdInstMod"
        # msgbox("It's Alive! - CmdInstMod", "Event: OnClick", "info")
        cons = self.txtConsole
        cons.TextColor = -1
        modtxt = self.txtPkg.Text
        if modtxt.strip() == "":
            cons.TextColor = 0xff0000
            cons.Text = "Please provide a package name.\n"
            self.DialogContainer.getControl("txtPkg").setFocus()
            return
        self.disableDialog()
        cons.Text = "Install Package in progress. Please wait ...\n"
        try:
            self.logger.debug(f'subprocessing <pip install --user --no-warn-script-location {modtxt}> '
                              f'(pythonexe: {self.pythonexe}, shell: {self.shell})')
            output = subprocess.check_output(
                [self.pythonexe, "-m", "pip", "install", "--user", "--no-warn-script-location", modtxt],
                stderr=subprocess.STDOUT, shell=self.shell)
            cons.Text = output
            # workaround for weird EXT_SUFFIX on some systems
            if platform.system() != "Windows":
                from sysconfig import get_config_var
                extsuffix = get_config_var("EXT_SUFFIX")
                dist = distribution(modtxt)
                if any(str(c).endswith(".so") and not str(c).endswith(extsuffix) for c in dist.files):
                    self.logger.debug('Start "workaround for weird EXT_SUFFIX on some systems"')
                    self._symlinkbinaries(extsuffix, dist)
        except subprocess.CalledProcessError as e:
            cons.Text = e.output
        finally:
            self.enableButtons()
        # TODO: not implemented

    def cmdUninstPkg_OnClick(self):
        self.t = Thread(target=self.workerCmdUninstPkg)  # , args = (doc,))
        self.t.start()

    def workerCmdUninstPkg(self):
        cons = self.txtConsole
        cons.TextColor = -1
        modtxt = self.txtPkg.Text
        if modtxt.strip() == "":
            cons.TextColor = 0xff0000
            cons.Text = "Please provide a package name below.\n"
            self.DialogContainer.getControl("txtPkg").setFocus()
            return
        self.disableDialog()
        cons.Text = "Uninstall Package in progress. Please wait ..."
        # self.DialogModel.Title = "It's Alive! - CmdUninstMod"
        # msgbox("It's Alive! - CmdUninstMod", "Event: OnClick", "info")
        try:
            self.logger.debug(f'subprocessing <pip uninstall -y {modtxt}> '
                              f'(pythonexe: {self.pythonexe}, shell: {self.shell})')
            output = subprocess.check_output(
                [self.pythonexe, "-m", "pip", "uninstall", "-y", modtxt], stderr=subprocess.STDOUT, shell=self.shell)
            cons.Text = output
            # invalidate cached copy if pip itself is uninstalled
            if modtxt.lower() == "pip":
                sys.modules.pop("pip")
                self.txtPkg.Text = ""
        except subprocess.CalledProcessError as e:
            cons.Text = e.output
            return
        finally:
            self.enableButtons()

    def _symlinkbinaries(self, extsuffix, dist):
        from site import USER_SITE
        distinfospath = str(dist._path)
        rgx = re.compile(r"\.cpython-[^.]*[darwin|gnu]\.so$")
        with open(os.path.join(distinfospath, "RECORD"), 'r+') as record:
            lines = record.readlines()
            for line in lines:
                filepath = line.split(',')[0]
                if rgx.search(filepath):
                    try:
                        linkpath = rgx.sub(extsuffix, filepath)
                        os.symlink(os.path.join(USER_SITE, filepath), os.path.join(USER_SITE, linkpath))
                        record.write(linkpath + '\n')   # needed to have symlinks removed on pip unstall command
                        self.logger.debug(f'symlink created: {os.path.join(USER_SITE, linkpath)}.')
                    except FileExistsError:
                        pass


def isPipInstalled():
    try:
        import pip
        return True
    except ImportError:
        return False


def find_python():
    for path in sys.path:
        try:
            for file in os.listdir(path):
                if re.match('^python([2-3]|.exe)?$', file, re.I):
                    assumed_path = os.path.join(path, file)
                    if os.path.isfile(assumed_path):
                        # print(assumed_path)
                        return assumed_path
        except Exception:
            continue
    # print(sys.executable)
    return sys.executable


def run_dlgPip(ctx, strings, logger):
    try:
        ctx = remote_ctx    # IDE
    except NameError:
        pass    # UI
    app = dlgPip(ctx, strings, logger)
    app.showDialog()
