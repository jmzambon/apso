Le classeur exemple **APSOwithPip_Example_Numpy.ods** permet de tester le module **numpy** (c'est une bibliothèque pour Python, destinée à manipuler des matrices ou tableaux multidimensionnels ainsi que des fonctions mathématiques opérant sur ces tableaux) dans une macro en python. Dans ce classeur on génére des entiers aléatoires en utilisant la fonction python génératrice de tableaux **numpy.random.randint**.

Le classeur est constitué de deux feuilles :

La feuille **Random** affiche les résultats et comporte deux boutons qui lancent des macros en python (embarqués dans le classeur). Le premier bouton **Clear Data** permet d'effacer le contenu des cellules de la plage définie dans le paramètre **Range to clear** de la feuille **Params**. Le deuxième bouton permet de générer des entiers aléatoires en utilisant les paramètres de la feuille **Params**.


La feuille **Params** contient les paramètres pour la feuille **Random** :

**int Alea Max**	contient l'entier maximum qui sera généré par le générateur aléatoire d'entiers

**nb col**	contient le nombre de colonnes à remplir par les entiers aléatoires

**nb row**	contient le nombre de lignes à remplir par les entiers aléatoires

**Dest**	contient la cellule de départ de remplissage des entiers alétoires

**Range to clear**  contient la plage dont on veut effacer le contenu par le bouton Clear Data.

Chaque paramètre correspond à une cellule nommée.

Pour que les macros fonctionnent il faut installer le paquet **numpy**.

Le classeur exemple a été testé  avec LibreOffice 7.x sous Windows 11, Ubuntu 20.04, MacOs 10.15.

![example_apsowp](img/Win11ApsoPipEx.gif)


