# APSO<br><br>Gestionnaire de script Python alternatif pour LibreOffice

Basée sur un [script original de Hanya](https://forum.openoffice.org/en/forum/viewtopic.php?p=242221#p242221), APSO est une extension qui permettra d'installer un gestionnaire de macros dédié aux scripts python.

---

### Contenu
- [Prérequis](#prérequis)
- [Caractéristiques](#caractéristiques)
- [Limitations](#limitations)
- [Comment l'utiliser](#comment-lutiliser)
- [Aides](#aides)
- [Options](#options)
- [Débogueur](#débogueur)
- [Gestionnaire de paquets (bêta)](#gestionnaire-de-paquets-module-bêta)
- [Traduction](#traduction)
---

### Prérequis
Sur certains systèmes (comme Ubuntu), le fournisseur de script Python pour LibreOffice n'est disponible que sous forme de package séparé (voir [ici](https://wiki.documentfoundation.org/Macros/Python_Guide/Introduction#Installation)).

### Caractéristiques
- localisation (EN, DE, FR, IT, HU grâce à Zizi64, NL grâce à Dick Groskamp, ES grâce à Francisco Gracia, JA grâce à nogajun et PT grâce à Rafael Lima)
- localisation de la gestion des paquets (EN, FR)
- intégration et extraction de scripts à partir de documents
- lier l'extension avec son éditeur de code préféré (grâce à [un autre script de Hanya](http://hermione.s41.xrea.com/pukiwiki/index.php?OOobbs2%2F194&word=editorkicker), voir Options ci-dessous)
- édition rapide des scripts embarqués (mais uniquement dans le cadre de l'extension APSO, attention ici)
- mise à jour automatique une fois installé
- menu contextuel pour un accès rapide aux actions des nœuds (à partir de 1.3.0)
- [bêta] gestionnaire de paquets (grâce à Jurassik Pork)
- [bêta] aide de base sur les objets UNO depuis la console Apso, via une version étendue de la fonction help() interne à python (à partir de v1.4.4) 

### Limitations
- Les scripts python ne peuvent pas être embarqués dans un document au format "flat xml" (voir l'issue #49). 

### Comment l'utiliser
Téléchargez et installez la dernière version comme n'importe quelle autre extension : double-cliquez sur le fichier oxt ou importez-le depuis le gestionnaire d'extensions (menu *Outils -> Extensions...*), puis redémarrez le programme.

L'extension ajoute un nouvel élément *Gérer les scripts python* sous le menu *Outils/Macros* avec le raccourci par défaut *Alt+Shift+F11* :

![apso_menu](img/apso_menu_fr.png)

Ce nouvel élément ou raccourci ouvre une boîte de dialogue qui affiche tous les scripts Python et propose diverses actions :

![apso_dialog](img/apso_dialog_fr.png)

Le bouton **Exécuter** lance la macro sélectionnée.
Les actions disponibles sous le bouton **Menu** changent en fonction de la sélection :
- créer un module ou une bibliothèque si la sélection est un conteneur (*Mes Macros*, *Office Macros* ou *document*)
- Modifier, renommer, supprimer un module ou une bibliothèque
- Intégrer le module de l'application (*Mes macros*, *Office Macros*) dans le document actuel
- Remplacer un module embarqué par un fichier externe
- Exporter un module embarqué

### Aides
APSO est livré avec une petite bibliothèque « apso_utils » qui contient des aides à des fins de débogage lors de l'écriture de macros.
Ces assistants sont quatre fonctions nommées *msgbox*, *xray*, *mri* et *console* et peuvent être importés par une instruction d'importation classique comme
```python
from apso_utils import msgbox
```

fonction | description
:---: | ---
boîte de message | Comme la fonction intégrée oobasic *msgbox*, mais simplifiée car uniquement destinée au débogage rapide.<br>**Signature** : `msgbox(message, title='Message', boxtype='message', boutons=1, gagner=Aucun)`
xray | Lance l'outil d'introspection [Xray](http://www.openoffice.org/fr/Documentation/Basic/) de Bernard Marcelly (doit être installé au préalable).<br>**Signature** : `xray(obj)`
MRI | Lance l'outil d'introspection [MRI](https://extensions.openoffice.org/en/project/MRI) par Hanya (doit être installé au préalable).<br>**Signature** : `mri(target)`
console | Émule, aussi fidèlement que possible, le shell interactif python. Ce qu'on peut faire avec un tel outil :<br />- interagir directement avec un document et tester "en direct" les méthodes de l'api uno (peut-être aussi utile pour d'autres langages, par exemple basiques) ;<br />- vérifier rapidement python expressions dans le contexte de l'interpréteur embarqué ;<br />- afficher la sortie d'un script exécuté en parallèle ou démarré depuis le shell (tous les modules visibles depuis le programme bureautique peuvent être importés) ;<br />- déboguer un script en utilisant La fonction runcall de pdb (ne fonctionnera pas avec les scripts intégrés dans les documents) ;<br />- obtenir des informations rapides sur n'importe quelle fonction à l'aide de la commande help() intégrée (à partir de v1.4.4 pour les objets UNO) ;<br />- explorer les objets vivants avec des outils d'introspection tels que le MRI ou Xray (si installé);<br />- ....<br>**Signature** : `console(**kwargs)`, où les *kwargs* facultatifs sont destinés à écraser les valeurs par défaut de l'interface graphique (ARRIÈRE-PLAN, PREMIER PLAN.. .) ou pour transmettre les paramètres régionaux et/ou globaux de l'appelant au contexte de la console.


**_Exemple_**
```python
depuis la console d'importation apso_utils, msgbox, mri, xray
def testhelpers() :
     ##console simple :
     # console()

     ## console noir et blanc :
     # console(FOND=0x0, AVANT=0xFFFFFF)

     ## passe la fenêtre parent pour obtenir la console supérieure non modale :
     # doc = XSCRIPTCONTEXT.getDocument()
     # console(parent=doc.CurrentController.ComponentWindow)

     ## charge les variables actuelles dans l'espace de noms de la console :
     loc = locaux()
     loc.update(globales())
     console(loc=loc)

     ## outils d'introspection et boîte de message :
     doc = XSCRIPTCONTEXT.getDocument()
     # xray(doc)
     msgbox(doc.Titre)
     MRI (doc)
```

<img src="img/apso_console_fr.png">

**_Conseils_**

- La macro *console* est accessible sans ouvrir la boîte de dialogue APSO.

- Si quelqu'un souhaite importer d'autres assistants hors de la boîte de dialogue APSO, il doit instancier le service APSO :
```python
def Entryfunc (événement = Aucun) :
     ctx = XSCRIPTCONTEXT.getComponentContext()
     ctx.ServiceManager.createInstance("apso.python.script.organizer.impl")
     # maintenant nous pouvons importer la bibliothèque apso_utils
     from apso_utils import MRI
     MRI (ctx)
```

### Options
La page des options d'extension est disponible sous *Outils -> Extensions -> APSO -> Options*.

Par défaut, APSO utilise l'éditeur défini par le système. Pour éditer des scripts avec un éditeur spécifique, indiquez le chemin complet de l'exécutable dans le champ "Editeur". Certains éditeurs MacOS semblent se comporter différemment ; si vous rencontrez un problème, veuillez vous référer à ce [post](https://forum.openoffice.org/fr/forum/viewtopic.php?p=363065&sid=1e0fbeb9185ee44751ef271b87b11fad#p363065) dans le forum français OpenOffice/LibreOffice.

Pour permettre l'ouverture à un décalage de ligne et de colonne donné le cas échéant, saisissez dans le champ "Options" la syntaxe de ligne de commande correspondant à l'éditeur choisi, en utilisant les espaces réservés {FILENAME}, {ROW} et {COL} (ils seront remplacés en temps voulu).
Exemple pour Emacs : `+{ROW}:{COL} {FILENAME}`.
Exemple de texte sublime : `{FILENAME}:{ROW}:{COL}.`

Une troisième option permet de spécifier le contenu par défaut sur tout nouveau module.

Une quatrième option permet d'activer le gestionnaire de paquets pip (expérimental).

Trois options avancées sont disponibles dans la section *Configuration experte* de LibreOffice (menu *Outils -> Options -> LibreOffice -> Avancé*) :
- `SaveLastScript`, si vrai, conserve l'historique du dernier script exécuté  ;
- `SaveTreeState`, si vrai, conserve l'historique de tous les nœuds développés lors du dernier script exécuté  ;
- `UnloadChildOnCollapse`, si vrai, réduit le nœud demandé et tous ses enfants ; si faux, réduit uniquement le nœud demandé  ;

### Débogueur
Apso est livré avec un débogueur de base intégré :

![apso_menu](img/apso_debug_fr.png)

Le débogueur peut être lancé à partir du bouton Apso **Menu** lorsqu'une fonction est sélectionnée dans la fenêtre principale.

Une fois le débogueur ouvert, on peut exécuter le code étape par étape, saisir n'importe quelle fonction appelée, revenir à l'appelant, etc.

Les points d'arrêt ne sont pas pris en charge, mais vous pouvez avancer en double-cliquant sur n'importe quelle ligne située en dessous de la ligne actuelle dans la même fonction.

Vous pouvez vérifier l'état et la portée de l'appelant en double-cliquant dessus dans la fenêtre "Pile" (en bas à gauche). Si MRI ou Xray sont installés, vous pouvez inspecter toutes les variables UNO en double-cliquant dessus dans la fenêtre "Scope" (en bas à droite).

Lorsque vous ouvrez une fenêtre de console à partir de la barre d'outils, les portées globale et locale seront automatiquement chargées.

Si vous modifiez le script alors que le débogueur est en cours d'exécution, les modifications seront prises en compte après avoir cliqué sur "Redémarrer".

Le débogueur utilise le nom de police et la hauteur définis pour l'EDI de base.

### Gestionnaire de paquets (module bêta)
Le gestionnaire de paquet n'est disponible qu'une fois l'option activée depuis la boîte de dialogue d’options d’Apso (si l'option n'est pas visible, agrandissez la fenêtre vers le bas).
Cette fonction a été proposée et développée par Jurassik Pork, que je remercie chaleureusement.

>**Note importante**. Le gestionnaire de paquets installe les paquets (pip inclus) dans le répertoire _site-packages_ de l’utilisateur en cours (voir ```from site import USER_SITE```). Ce répertoire se trouve en dehors du profil utilisateur de LibreOffice. Autrement dit, les paquets installés ne seront pas automatiquement supprimés lors d’une désinstallation d’Apso ou de LibreOffice.

On accède à ce gestionnaire en cliquant sur le dernier élément du menu APSO : **Gestion des paquets**.

Une fenêtre de gestion des paquets s'ouvre alors :

![apso_dlgpip](img/apso_dlgpip_fr.png)

Le bouton **Installation de pip** permet d'installer **pip** si celui-ci n’est pas déjà installé. Un message d’erreur apparaît dans la zone console texte de la fenêtre indiquant que **pip** n’est pas installé si c’est le cas.

Le bouton **Lister paquets** permet d'afficher les paquets qui sont déjà installés. On voit alors le nom des paquets ainsi que la version installée.

Le bouton **Installer paquet** permet d'installer le module qui est indiqué dans le champ **Paquet**. Par exemple pour installer le paquet numpy on tape numpy dans le champ Paquet. On peut aussi indiquer la version du paquet que l’on veut installer. Par exemple pour installer la version 1.5.4 du paquet numpy il faut taper numpy==1.5.4   
 
Le bouton **Désinstaller Paquet** permet de désinstaller le module qui est indiqué dans le champ **Paquet**.  

Le bouton **OK** permet de fermer la fenêtre  **Gestionnaire de paquets**.

Le bouton **Effacer** permet d'effacer le contenu de la console texte.

### Traduction
Si vous souhaitez contribuer à la traduction d'Apso dans votre langue, référez-vous au fichier **[apso_localizer.ods](apso_localizer.ods)** : toutes les chaînes de caractères à traduire y sont rassemblées, avec comme exemples les langues déjà disponibles.